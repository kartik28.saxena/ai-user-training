<u>**Deep Neural Network**</u><br><br>
<centre><img src = "https://i.pinimg.com/originals/b5/d3/69/b5d3692a872936d05a3d770e5327c6ec.gif" width="750" height="250"></centre>
<br>
- Neural Network refers to systems of neurons.
- It tries hard to recognize the relationships in a set of data through a process that mimics the human brain operates.


<br><br>**Layers of Neural Network**

<img src ="https://miro.medium.com/max/1826/1*L9xLcwKhuZ2cuS8fF0ZjwA.png" width="750">

There are three layers i.e.
1. *Input layer*: It consists of artifical input neurons that brings the initial data into the system.
2. *hidden layer*: Its function is to applies weight to the inputs and directs them through an activation funvtion.
3. *output layer*: Produces the result for given inputs.

<br><br>**How Do Neural Network Works?**

<img src = "https://machinelearningknowledge.ai/wp-content/uploads/2019/10/Backpropagation.gif">
<br>
A. *Forward Propogation*

- From left to right
- The weighted sum of each input node is calculated
- This is then passed to hidden layer where activation funvtion is applied to obatined a specific type of result.
- The data transfer through multiple hidden layers and specific nodes.
- At last an output value is retrived.
<br>

B. *Gradient Descent*

- The output value retrived is then compared with actual, hence finding how much error occured which is known as cost function.
- To reduce the cost function "Gradient Descent" algorithm is used.
- It is an iterative optimization algorithm.
- It reduces the cost function by iteratively moving in the direction of the steepest descent defined as negative of the gradient.
<br>

C. *Back Propogation*
 
- From right to left
- The error is now back Propogated.
- According to the error, weights are updated.
- The learning rates decide how much we update the weights

**NOTE**: When the whole data is passed through the DNN, that makes an epoch.
          Redo more epoch to get the best results.




