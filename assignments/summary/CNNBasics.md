**Convolutional Neural Network**

- It is a part of deep learning and class of deep neural network.
- It is used to classify different types of images.
- The key here is *The Features* of images.
- It decresases the size of image causes loss of data but it focuses on features that helps to distinguish different images.
![image](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/75_blog_image_1.png)

**Steps to perform CNN:**

1. **Convolution**
    
    ![image](https://i.stack.imgur.com/uEoXw.gif)
    - First we create *Filter matrix* or *Feature Detector*.It is the maximum features that we can obtain from input image. The size of  
      Filter Matrix is known as *Kernel Size*(K) which is in pixel and in general, small and odd size of it is better.                
    - Now we develop a *Feature Map* by convolve the Filter  through image with a particular *stride*. Stride is number of the pixels by which a
      Filter Matrix moves, it is set as 1 so that no location are missed from the image.
    - Reduces the size of image.
    - Convolution makes image linear to some extend and to reduce it we use *Rectifier Function*, this layer is known as *ReLU Layer*.



2. **Max Pooling***
    
    
    ![image](https://computersciencewiki.org/images/8/8a/MaxpoolSample2.png)
    - Now the Feature Map obtained from Convolution is used for Max Pooling.
    - It provides flexbility in detecting features.
    - It captures all the main features disregard of where it is present.
    - The Feature Map produced after Max Pooling is known as *Pooled Feature Map*. 
    - Reduces size and prevent *overfitting*.   



3. **Flattening**
    
    
    
    ![image](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/73_blog_image_1.png)
    - The Pooled Feature Map obtained from Max Pooling is used for Flattening.
    - Here the matrix of features are flattened in form of single vector which can be used as an Input Layer of a future DNN.



4. **Full Connection**
    
    
    
    ![image](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/74_blog_image_1.png)
    - Here all neurons are connected with the previous layer.
    - It takes the flattened single vector as an input layer and provide an *Output* in term of probability.
    - If error is found then the *weights* and *features* are adjusted.


